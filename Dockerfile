ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=pythonpackagesonalpine
ARG DOCKER_BASE_IMAGE_NAME=basic-python-packages-pre-installed-on-alpine
ARG DOCKER_BASE_IMAGE_TAG=pip-alpine
FROM ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:${DOCKER_BASE_IMAGE_TAG}

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    # can build ovirt-engine-sdk-python here by installing libxml2-dev
    && apk add --no-cache --virtual .build-dependencies build-base libxml2-dev \
    # py3-libxml2 is available but pointless since we still need to build ovirt-engine-sdk-python itself
    # && apk add --no-cache py3-libxml2 \
    && python -m pip install --no-cache-dir ovirt-engine-sdk-python \
    && apk del --no-cache .build-dependencies \
    && apk add --no-cache libxml2 \
    && python -c "import ovirtsdk4" \
    && . ./cleanup.sh
